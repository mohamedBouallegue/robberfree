<?php

 namespace App\notification;
use App\notification\smsnotification;
use App\notification\emailnotification;

 /**
 * 
 */
 class notification
 {
 	private $object=NULL;
 	public function __construct($notificationType)
 	{
 		switch ($notificationType) {
 			case 'sms':
 				$this->object= new smsnotification(); 
 		
 				break;
 			
 			case 'email':
 				$this->object= new emailnotification();
 				break;
 				
 		}
 	}

 	public function notifyUser()
 	{
 		$this->object->notify();
 	}
 }




?>