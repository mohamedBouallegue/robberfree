<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/home/{id}', 'cameraController@store');

Route::get('/payment', function () {
    return view('payment');
});

Route::post('/payment', 'paymentController@store');



Route::post('/notification/{id}', 'notificationController@store');



Route::get('/notification/{id}', 'notificationController@getRecord');


Route::get('/cameralist/{id}', 'cameraController@list');

Route::get('/account/{id}', 'userController@getuserinformation');

Route::get('/hungary', 'cameraController@test');