
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/simple-sidebar.css')}}" rel="stylesheet">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">
<link href="{{asset('css/bootstrap-form-helpers.min.css')}}" rel="stylesheet">


</head>

<body>
 <img src="person.jpeg" alt="user" style="width:50px;  border-radius: 90%;">

            <ul class="sidebar-nav">
                <li> 
                    <a href="/cameralist/{{ Auth::user()->id }}" ><i class="fa fa-camera"></i><span>  Surveillance</span> </a>
                    
                </li> 
                <li>
                    <a href="/notification/{{ Auth::user()->id }}"><i class="fa fa-bell-o"></i>   Notification</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-list"></i>     Prefrences</a>
                </li>
                <li>
                    <a href="/account/{{ Auth::user()->id}}"><i class="fa fa fa-gears"></i>  Account  </a>
                    <ul class="sub-menu">
                
            </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa fa-bar-chart"></i>  Reports</a>
                </li>
                <li>
                    <a href="/payment"><i class="fa fa-credit-card"></i>  Billing</a>
                </li>
                
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
<ol class="breadcrumb" style="padding-left: 300px; height: 70px; padding-top: 20px;"> 
  <li class="breadcrumb-item" ><a href="#" style="color: #73879C;">Home</a></li>
  <li class="breadcrumb-item active">Account</li>
 <div style="padding-left: 600px;"> 
    <input type="text" class="form-control" placeholder="Search">
  </div>
 <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                   <span class="caret"> {{ Auth::user()->name }} </span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                </li>
</ol>


<form method="POST" action="/notification/{{ Auth::user()->id }}" style=" margin-top: 110px;">
{{ csrf_field() }}

@if ($errors->has('notificationchoice'))
                                   

                                <div class="alert alert-dismissible alert-warning" style="max-width: 35rem; margin-left: 570px; ">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Warning!</h4>
  <p class="mb-0">{{ $errors->first('notificationchoice') }} </p>
</div>
 @endif

<div class="card border-light mb-3" style="max-width: 35rem; margin-left: 570px;">
  <div class="card-header"><h4>Notification Settings</h4></div>
  <div class="card-body">

  <h5>Notification Method:</h5>
     <div class="form-group" style="margin-left: 190px; margin-top: -30px">
    <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" id="customCheck1"  name="notificationchoice[]" value="email"  @if(in_array("email", $notificationmethod)) checked @endif >

      <label class="custom-control-label" for="customCheck1" >Email</label>
    </div>

      <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" id="customCheck2" name="notificationchoice[]" value="sms" @if(in_array("sms", $notificationmethod)) checked @endif> 
            
      <label class="custom-control-label" for="customCheck2">SMS</label>
    </div>
 
    <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" id="customCheck3" name="notificationchoice[]" value="call" @if(in_array("call", $notificationmethod)) checked @endif>
      <label class="custom-control-label" for="customCheck3">Phone Call</label>
    </div>

    <button type="button" class="btn btn-primary" style="margin-left: 10px; width: 80px; margin-top: 20px;">Cancel</button>
     
     <button type="submit" class="btn btn-primary" style=" margin-top: 20px">Submit  </button>   
    </div>
  </div>
  


                                </div>

  </form>




    <!-- Bootstrap core JavaScript -->
     <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-formhelpers.min.js')}}"></script>
   
    <!-- Menu Toggle Script -->
   
</body>

</html>



