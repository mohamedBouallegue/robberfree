
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>payment</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/simple-sidebar.css')}}" rel="stylesheet">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">
<link href="{{asset('css/bootstrap-form-helpers.min.css')}}" rel="stylesheet">


</head>

<body>
 <img src="person.jpeg" alt="user" style="width:50px;  border-radius: 90%;">

            <ul class="sidebar-nav">
                <li> 
                    <a href="/cameralist/{{ Auth::user()->id }}" ><i class="fa fa-camera"></i><span>  Surveillance</span> </a>
                    
                </li> 
                <li>
                    <a href="/notification/{{ Auth::user()->id }}"><i class="fa fa-bell-o"></i>   Notification</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-list"></i>     Prefrences</a>
                </li>
                <li>
                    <a href="/account/{{ Auth::user()->id}}"><i class="fa fa fa-gears"></i>  Account  </a>
                    <ul class="sub-menu">
                
            </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa fa-bar-chart"></i>  Reports</a>
                </li>
                <li>
                    <a href="payment"><i class="fa fa-credit-card"></i>  Billing</a>
                </li>
                
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
<ol class="breadcrumb" style="padding-left: 300px; height: 70px; padding-top: 20px;"> 
  <li class="breadcrumb-item" ><a href="/home" style="color: #73879C;">Home</a></li>
  <li class="breadcrumb-item active">Payment</li>
 <div style="padding-left: 600px;"> 
    <input type="text" class="form-control" placeholder="Search">
  </div>
 <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                   <span class="caret"> {{ Auth::user()->name }} </span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                </li>
</ol>


<form method="post" action="payment">
{{ csrf_field() }}
     <div class="container" style="margin-left: 200px">
    <div class="row" >
        <div class="col-xs-12 col-md-4 offset-md-4" >
            <div class="card " >
                <div class="card-header" style="height: 70px">
                    <div class="row">
                        <h5>Payment Details</h5>
                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png" style="margin-left: 140px; margin-top: -10px;">
                    </div>
                </div>
                <div class="card-block">
                    <form role="form">

                      <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group" style="margin-left: 10px">
                                    <label><span class="hidden-xs">Number of Hours</span></label>
                                    <input type="tel" class="form-control" placeholder="10" name="nbhours"  value="{{ old('nbhours') }}"/>
                                </div>

                                @if ($errors->has('nbhours'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nbhours') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="col-xs-5 col-md-5 float-xs-right">
                                <div class="form-group" style="margin-right: 10px">
                                    <label>Costs</label>
                                        <input class="form-control" id="readOnlyInput" type="text" placeholder="40dt" readonly="" name="costs" >

                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group" style="margin-left: 30px">
                                    <label>Card Number</label>
                                    <div class="input-group">
                                        <input type="tel" class="form-control" placeholder="Valid Card Number" name="cardnumber" value="{{ old('cardnumber') }}"/>
                                        <span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
                                    </div>
                                      @if ($errors->has('cardnumber'))
                                    <span class="help-block"  >
                                        <strong>{{ $errors->first('cardnumber') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group" style="margin-left: 10px">
                                    <label><span class="hidden-xs">Expiration</span> Date</label>
                                    <input type="tel" class="form-control" placeholder="MM / YY" name="date" value="{{ old('date') }}"/>
                                </div>
                                  @if ($errors->has('date'))
                                    <span class="help-block"  >
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-5 col-md-5 float-xs-right">
                                <div class="form-group" style="margin-right: 10px">
                                    <label>CV Code</label>
                                    <input type="tel" class="form-control" placeholder="CVC" name="cvc" value="{{ old('cvc') }}"/>
                                </div>
                                  @if ($errors->has('cvc'))
                                    <span class="help-block"  >
                                        <strong>{{ $errors->first('cvc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group" style="margin-left: 30px">
                                    <label>Card Owner</label>
                                    <input type="text" class="form-control" placeholder="Card Owner Names" name="ownername" value="{{ old('ownername') }}"/>
                                </div>
                                  @if ($errors->has('ownername'))
                                    <span class="help-block"  >
                                        <strong>{{ $errors->first('ownername') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer" >
                    <div class="row" style="margin-left: 75px;">
                        <div class="col-xs-12">
                            <button class="btn btn-warning btn-lg btn-block" type="submit">Process payment</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<style>
    .cc-img {
        margin: 0 auto;

    }
</style>



    <!-- Bootstrap core JavaScript -->
     <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-formhelpers.min.js')}}"></script>
   
    <!-- Menu Toggle Script -->
   
</body>

</html>



