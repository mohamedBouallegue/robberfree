<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign in</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">
    <style>
        #box{
            
            width: 350px;
            margin: auto;   
            margin-top: 50px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">RobberFree</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
     <a href="/login" class="nav-bottom-link" style="margin-right: 30px;">
<span class="nav-bottom-title">SIGN IN</span>
</a>

<a href="/register" class="nav-bottom-link" style="margin-right: 70px;">
<span class="nav-bottom-title">SIGN UP</span>
</a>

    </form>
  </div>
</nav>

<div id="box">
<h3 class="display-5" style="margin-left: 40px;margin-bottom: 40px;">Join RobberFree</h3> 
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="form-group">

                                <input type="email" class="form-control" id="email1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{ old('email') }}" required>
                                
                            </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                       

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label style="display:inline">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} > Remember Me
                                    </label>
                                    
                                </div>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                         

                        <div class="form-group">
                            
                                <div style="margin-top: 30px;">
                                     <button type="cancel" class="btn btn-primary" style="margin-left: 80px; margin-right: 5px; width: 100px;">cancel</button>
                                      <button type="submit" class="btn btn-primary " style="width: 100px;">Sign in</button>

                                    </div>
                                    <div class="list-group">
                                    <p class="list-group-item " class="text-primary" style="padding-left: 35px; margin-top: 45px;  height:50px;">New to RobberFree? <a href="register">Create an account</a> 
                                </div>

                                
                            
                        </div>
                    </form>
                    <div class="footer-copyright py-3 text-center" style="margin-top: 120px;">
        © 2018 Copyright:
        <a href="https://RobberFree.com">
            <strong> RobberFree.com</strong>
        </a>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>

