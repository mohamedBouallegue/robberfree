<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign in</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">
    <style>
        #box{
            
            width: 350px;
            margin: auto;   
            margin-top: 50px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">RobberFree</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      
      <a href="/login" class="nav-bottom-link" style="margin-right: 30px;">
<span class="nav-bottom-title">SIGN IN</span>
</a>

<a href="/register" class="nav-bottom-link" style="margin-right: 70px;">
<span class="nav-bottom-title">SIGN UP</span>
</a>
      

    </form>
  </div>
</nav>
<div id="box">
<h3 class="display-5" style="margin-left: 40px;margin-bottom: 40px;">Join RobberFree</h3> 
    
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    
                             <div class="form-group">
                                 <input type="text" class="form-control" id="name"  name="name" placeholder="Full name" value="{{ old('name') }}" required autofocus>
                             </div> 
                                
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            
                             <div class="form-group">

                                <input type="email" class="form-control" id="email1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{ old('email') }}" required>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>

                            </div>
                                
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="form-group">
      
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>

                           

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Confirm Password" required>

                        </div>

                       

                        <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="customCheck1" required>
                                  <label class="custom-control-label" for="customCheck1">Agree on the privacy policy and term of use </label>
                                </div>
                                </div>
                                <div style="margin-top: 30px;">
                                 <button type="cancel" class="btn btn-primary" style="margin-left: 80px; margin-right: 5px; width: 100px;">cancel</button>
                                  <button type="submit" class="btn btn-primary " style="width: 130px;">Create account</button>
                                </div>
                 
                
                <div class="footer-copyright py-3 text-center" style="margin-top: 35px;">
        © 2018 Copyright:
        <a href="https://RobberFree.com">
            <strong> RobberFree.com</strong>
        </a>
    </div>
    </div>
            </div>
            </form> 
            </body>
            </html>
    
