
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>account setting</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/simple-sidebar.css')}}" rel="stylesheet">

   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">
<link href="{{asset('css/bootstrap-form-helpers.min.css')}}" rel="stylesheet">


</head>

<body>
 <img src="person.jpeg" alt="user" style="width:50px;  border-radius: 90%;">

            <ul class="sidebar-nav">
                <li> 
                    <a href="/cameralist/{{ Auth::user()->id }}" ><i class="fa fa-camera"></i><span>  Surveillance</span> </a>
                    
                </li> 
                <li>
                    <a href="/notification/{{ Auth::user()->id}}"><i class="fa fa-bell-o"></i>   Notification</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-list"></i>     Prefrences</a>
                </li>
                <li>
                    <a href="/account/{{ Auth::user()->id }}"><i class="fa fa fa-gears"></i>  Account  </a>
                    <ul class="sub-menu">
                
            </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa fa-bar-chart"></i>  Reports</a>
                </li>
                <li>
                    <a href="/payment"><i class="fa fa-credit-card"></i>  Billing</a>
                </li>
                
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
<ol class="breadcrumb" style="padding-left: 300px; height: 70px; padding-top: 20px;"> 
  <li class="breadcrumb-item" ><a href="#" style="color: #73879C;">Home</a></li>
  <li class="breadcrumb-item active">Account</li>
 <div style="padding-left: 600px;"> 
    <input type="text" class="form-control" placeholder="Search">
  </div>
 <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                   <span class="caret"> {{ Auth::user()->name }} </span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                </li>
</ol>


     <div style="height: 500px; width: 500px; margin-left: 650px; margin-top: 70px; ">
  <div class="form-group">
  <label class="col-form-label" for="name" style="float: left; ">Full Name:</label>
  <input type="text" class="form-control" id="name" style="width: 300px;">
</div>

<div class="form-group">
  <label class="col-form-label" for="Email" style="float: left;">Email:</label>
  <input type="text" class="form-control" id="Email" style="width: 300px; margin-left: 85px">
</div>

<label class="col-form-label" for="countries1" style="float: left;">Country:</label>
<select id="countries1" class="form-control"  onclick="$('#countries1').bfhcountries({country: 'TN'})" style="width: 300px; margin-left: 85px; margin-bottom: 15px"></select>


<div class="form-group">
  <label class="col-form-label" for="address" style="float: left; ">Address:</label>
  <input type="text" class="form-control" id="address" style="width: 300px; margin-left: 85px">
</div>

<div class="form-group">
  <label class="col-form-label" for="company" style="float: left;">Company:</label>
  <input type="text" class="form-control" id="company" style="width: 300px; margin-left: 85px">
</div>

<button type="button" class="btn btn-primary" style="margin-left: 120px; margin-bottom: 100px">Save Profile</button>


</div>




    <!-- Bootstrap core JavaScript -->
     <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-formhelpers.min.js')}}"></script>
   
    <!-- Menu Toggle Script -->
   
</body>

</html>



