<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\notificationRequest;
use App\notification;
use Request;

class notificationController extends Controller
{
    public function store(notificationRequest $request)
    {
    
    	$notification=notification::firstOrCreate(['user_id' => Request::segment(2)]);
    	$notification->user_id=Request::segment(2);
		$notificationmethod = $request->input('notificationchoice');
		if(in_array("email", $notificationmethod))
 			$notification->email=1;
		else
			$notification->email=0;


		if(in_array("sms", $notificationmethod))
 			$notification->sms=1;
		else
			$notification->sms=0;


		if(in_array("call", $notificationmethod))
 			$notification->call=1;
		else
			$notification->call=0;

		$notification->save(); 

    	return view('notification')->with('notificationmethod', $notificationmethod);
    }


    public function getRecord(){

    	$notificationmethod=[];
    	$notification = notification::whereuser_id(Request::segment(2))->first();
    	if($notification['sms']==1)
    		$notificationmethod[]='sms';

    	if($notification['email']==1)
    		$notificationmethod[]='email';
    	
    	if($notification['call']==1)
    		$notificationmethod[]='call';
    	


    	return view('notification')->with('notificationmethod', $notificationmethod);

    }
}
