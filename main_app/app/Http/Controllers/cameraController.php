<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\addCameraRequest;
use App\Camera;
use Request;


class cameraController extends Controller
{
    public function store(addCameraRequest $request)
    {
       
       $camera = new Camera;

		$camera->name = $request->input('cameraname');
		$camera->ipaddress = $request->input('ipaddress');
		$camera->description = $request->input('description');
		$camera->user_id=Request::segment(2);
		$camera->save(); 

      return view('addCamera');
        
    }

    public function list(){

    	$cameraslist = Camera::where('user_id', Request::segment(2))
                        ->get();
      $cameras=[];
      foreach ($cameraslist as $cam) {
                          $cameras[]=$cam['name'].' '.$cam['ipaddress'];
                        }                  
                       
    	return view('cameraList')->with('cameras', $cameras);
    }

    
}
