<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
     protected $table = 'notification';
      protected $fillable = ['email', 'sms', 'call', 'user_id'];
}
