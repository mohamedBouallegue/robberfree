# RobberFree

## Requirements

* Docker CE 17.06 or later
* docker-compose
* php 7.0 or later
* composer

## Provisioning

### First time running environment: (fresh start)
```
$ docker-compose up -d
```
### Install dependencies
Run `composer install` in each of the service folders: main_app, watcher_api, payment_api, recognition_api, notification_api
### Run database migrations:
First you need to ssh into each php container
```
$ docker-compose exec watcher_api_php bash
cd ../site
php artisan migrate
```
The same steps needs to be done with all the other php containers.

After all these steps are done you can access the main page: http://localhost:8080/home

