<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/watch', function (Request $request) {
    $cameraId = $request->get('camera_id');
    $cameraUrl = $request->get('camera_url');
    $watchJob = new \App\Jobs\WatchCamera($cameraId, $cameraUrl);
    dispatch($watchJob);
    return 'id: ' . $cameraId . ' Url: ' . $cameraUrl;
});
