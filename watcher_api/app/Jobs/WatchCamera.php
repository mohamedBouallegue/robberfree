<?php

namespace App\Jobs;

use App\Services\RecognitionService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class WatchCamera implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var int
     */
    private $cameraId;
    /**
     * @var string
     */
    private $cameraUrl;

    /**
     * Create a new job instance.
     *
     * @param int $cameraId
     * @param string $cameraUrl
     */
    public function __construct(int $cameraId, string $cameraUrl)
    {
        $this->cameraId = $cameraId;
        $this->cameraUrl = $cameraUrl;
    }

    /**
     * Execute the job.
     *
     * @param RecognitionService $recognitionService
     * @return void
     */
    public function handle(RecognitionService $recognitionService)
    {
        $recognitionService->watchCamera($this->cameraUrl, $this->cameraId);
    }
}
