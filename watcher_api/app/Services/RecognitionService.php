<?php

namespace App\Services;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;

class RecognitionService
{
    /**
     * @var Client
     */
    private $client;


    /**
     * RecognitionService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function watchCamera($cameraUrl, $cameraId)
    {
        $filename = 'screenshot_' . $cameraId . '_%09d.jpg';
        $imageFolder = 'images_' . $cameraId;
        Storage::makeDirectory($imageFolder);

        $count1 = count(scandir(storage_path('app/' . $imageFolder)));
        sleep(1);
        $count2 = count(scandir(storage_path('app/' . $imageFolder)));
        if ($count1 === $count2) {
            $screenshotCommand = 'avconv -i ' . $cameraUrl . ' -r 1 -vsync 1  -qscale 1 -f image2 ' . storage_path('app/' . $imageFolder . '/' . $filename) . ' -y';
            $process = new Process($screenshotCommand);
            $process->start();
        }
        while (true) {
            $files = scandir(storage_path('app/' . $imageFolder));
            $lastScreenshot = array_pop($files);
            unset($files[0]);
            unset($files[1]);
            foreach ($files as $file) {
                if ($file !== '.' || $file !== '..' || $file !== '') {
                    unlink(storage_path('app/' . $imageFolder . '/' . $file));
                }
            }
            $isRobbery = $this->checkScreenshot(storage_path('app/' . $imageFolder . '/' . $lastScreenshot));
            if($isRobbery) {
                $this->sendResultToMainApp();
            }
        }
    }

    private function checkScreenshot($imagePath)
    {
        $result = $this->client->post('recognition_api_nginx/api/test', [
            'multipart' => [
                [
                    'name' => 'photo',
                    'contents' => fopen($imagePath, 'r')
                ]
            ]
        ]);
        unlink($imagePath);
        $body = $result->getBody()->getContents();
        $result = json_decode($body);

        return $result['Robbery'];
    }

    private function sendResultToMainApp($cameraId)
    {
        $this->client->post('main_app_nginx/api/robbery', [
            'cameraId' => $cameraId
        ]);
    }
}